package autospecs_TestCases;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pype.Autospecs.pageLibrary.LoginPage;
import com.pype.Autospecs.testbase.TestBase;

public class Project_Dashboard extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		init();
		selectBrowser("chrome");
	}

	@BeforeMethod
	public void login() throws Exception {
		LoginPage login = new LoginPage();
		login.LoginToAutospecs(driver);
		driverwait(10);
	}

	private static String getPatternGroupFromString(String text, String regex, Integer regexGroup) {
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(text);
		if (m.find()) {
			if (regexGroup != null && regexGroup != -1)
				return m.group(regexGroup);
			else
				return m.group();
		}
		return "";
	}

	@Test
	public void Dashboard_page() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 180);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Goto_Dashboard")))).click();
		Reporter.log("Navigated to Dashboard");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Select_project"))))
				.sendKeys(prop.getProperty("Project_Name_value"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("My_project")))).click();
		Thread.sleep(3000);
		WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Dashboard')]"));
		Thread.sleep(3000);
		String str = element.getText();
		Thread.sleep(5000);
		Assert.assertEquals("Dashboard", str);
		Reporter.log("Navigated to Dashboard page successfully");
		// Project submittals in Dashboard
		wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'Project Submittals')]")))
				.click();
		Thread.sleep(2000);
		str=SR_footer();
		Reporter.log("Project Submittals :" +str);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Goto_Dashboard")))).click();
		// Action/information submittals in dashboard
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//span[contains(text(),'Action/Informational Submittals')]")))
				.click();
		Thread.sleep(2000);
		str=SR_footer();
		Reporter.log("Action/Informational Submittals :" +str);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Goto_Dashboard")))).click();
		List<WebElement> webElements = driver
				.findElements(By.xpath("//*[@class='col-xs-6 actionsubmittals']//*[@class='tc-chart-js-legend']/li"));
		Reporter.log("Action/Informational Submittals : " + webElements.size());
		String s[] = new String[webElements.size()];
		for (int i = 0; i < webElements.size(); i++) {
			webElements = driver.findElements(
					By.xpath("//*[@class='col-xs-6 actionsubmittals']//*[@class='tc-chart-js-legend']/li"));
			s[i] = webElements.get(i).getText();
			Thread.sleep(2000);
			webElements.get(i).click();
			String numberOfSubmittalsFromDashBoard = getPatternGroupFromString(s[i],
					"([A-Za-z]*[\\s]*)([0-9][0-9]?[0-9]?)", 2);
			String numberOfSubmittalsFromFooter = SR_footer();
			if ((numberOfSubmittalsFromFooter.trim()).equalsIgnoreCase(numberOfSubmittalsFromDashBoard.trim())) {
				Reporter.log("Action/Informational Submittals and smart register records for " + s[i] + " count remains same");
	
			} else {
				Reporter.log("Action/Informational Submittals for " + s[i] + " are not matching with smart register records");
			
			}
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Goto_Dashboard"))))
					.click();

		}

		// Closeout submittals in Dashboard
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'Closeout Submittals')]"))).click();
		str=SR_footer();
		Reporter.log("Closeout Submittals :"+str);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Goto_Dashboard")))).click();
		List<WebElement> webElements1 = driver
				.findElements(By.xpath("//*[@class='col-xs-6 closeoutsubmittals']//*[@class='tc-chart-js-legend']/li"));
		Reporter.log("Closeout Submittals : " + webElements1.size());
		String s1[] = new String[webElements1.size()];
		for (int i = 0; i < webElements1.size(); i++) {
			webElements1 = driver.findElements(
					By.xpath("//*[@class='col-xs-6 closeoutsubmittals']//*[@class='tc-chart-js-legend']/li"));
			s1[i] = webElements1.get(i).getText();
			webElements1.get(i).click();
			Thread.sleep(2000);
			String numberOfSubmittalsFromDashBoard = getPatternGroupFromString(s1[i],
					"([A-Za-z]*[\\s]*)([0-9][0-9]?[0-9]?)", 2);
			String numberOfSubmittalsFromFooter = SR_footer();
			if ((numberOfSubmittalsFromFooter.trim()).equalsIgnoreCase(numberOfSubmittalsFromDashBoard.trim())) {
				Reporter.log("Closeout Submittals and smart register records for " + s1[i] + " count remains same");
			
			} else {
				Reporter.log("Closeout Submittals for " + s1[i] + " are not matching with smart register records");
			
			}
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Goto_Dashboard"))))
					.click();
		}
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='chartsData']//button"))).click();
		WebElement element1 = driver.findElement(By.xpath("//span[contains(text(),'Requirements Overview')]"));
		String str1 = element1.getText();
		Thread.sleep(1000);
		Assert.assertEquals("Requirements Overview", str1);
		Reporter.log("Successfully Navigated to Requirements Overview Page");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("View_dashboard")))).click();

		// Other requirements in dashboards
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//span[contains(text(),'Additional Requirements')]"))).click();
		str=SR_footer();
		Reporter.log("Additional Requirements :" +str);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Goto_Dashboard")))).click();
		List<WebElement> webElements2 = driver.findElements(By.xpath("//*[@class='otherrequirements-container']/li"));
		Reporter.log("Additional Requirements : " + webElements2.size());
		String s2[] = new String[webElements2.size()];
		for (int i = 0; i < webElements2.size(); i++) {
			webElements2 = driver.findElements(By.xpath("//*[@class='otherrequirements-container']/li"));
			s2[i] = webElements2.get(i).getText();
			webElements2.get(i).click();
			Thread.sleep(2000);
			Reporter.log(s2[i]);
			String numberOfSubmittalsFromDashBoard = getPatternGroupFromString(s2[i],
					"([A-Za-z]*[\\s]*)([0-9][0-9]?[0-9]?)", 2);
			String numberOfSubmittalsFromFooter = SR_footer();
			if ((numberOfSubmittalsFromFooter.trim()).equalsIgnoreCase(numberOfSubmittalsFromDashBoard.trim())) {
				Reporter.log("Additional Requirements and smart register records for " + s2[i] + " count remains same");
			
			} else {
				Reporter.log("Additional Requirements for " + s2[i] + " are not matching with smart register records");
			
			}

			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Goto_Dashboard"))))
					.click();
		}

	}

	@AfterClass
	public void closeBrowser() {
		driver.quit();
		Reporter.log("Browser Closed");

	}
}
