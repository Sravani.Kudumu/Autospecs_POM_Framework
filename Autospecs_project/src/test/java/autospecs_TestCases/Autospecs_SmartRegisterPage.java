package autospecs_TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.pype.Autospecs.pageLibrary.LoginPage;
import com.pype.Autospecs.testbase.TestBase;

public class Autospecs_SmartRegisterPage extends TestBase {
	@BeforeTest
	public void setUp() throws Exception {

		init();
		driver = selectBrowser("chrome");
	}

	@Test

	public void SmartRegister_page() throws Exception {
		LoginPage login = new LoginPage();
		login.LoginToAutospecs(driver);
		Reporter.log("Login successfully");
		WebDriverWait wait = new WebDriverWait(driver, 180);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Search_project"))))
				.sendKeys(prop.getProperty("Project_Name_value"));
		Reporter.log("Select project");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("SmartRegister")))).click();
		Reporter.log("Clicked on Smart Register");
		String str = SR_footer();
		Reporter.log("Total number of records in Smart Register :" + str);
		driver.findElement(By.xpath(prop.getProperty("SR_Add_button"))).click();
		driver.findElement(By.xpath(prop.getProperty("SR_Save_button"))).click();
		str = SR_footer();
		Reporter.log("Added one record successfully :" + str);
		Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("Actions_button"))).click();
		driver.findElement(By.xpath(prop.getProperty("Reset_submittal"))).click();
		driver.findElement(By.xpath(prop.getProperty("Reset_submittal_select"))).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("SR_Row1_Select"))).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(prop.getProperty("SR_Delete_button"))).click();
		driver.findElement(By.xpath(prop.getProperty("SR_Save_button"))).click();
		Thread.sleep(2000);
		str = SR_footer();
		Reporter.log("Deleted one record successfully :" + str);
		Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("Actions_button"))).click();
		driver.findElement(By.xpath(prop.getProperty("Reset_submittal"))).click();
		driver.findElement(By.xpath(prop.getProperty("Reset_submittal_select"))).click();
		Thread.sleep(1000);
	}

	@AfterClass
	public void closeBrowser() {
		driver.quit();
		Reporter.log("Browser Closed");

	}

}