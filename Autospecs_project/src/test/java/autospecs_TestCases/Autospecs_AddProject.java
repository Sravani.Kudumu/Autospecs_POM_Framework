package autospecs_TestCases;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.pype.Autospecs.pageLibrary.LoginPage;
import com.pype.Autospecs.testbase.TestBase;

public class Autospecs_AddProject extends TestBase {
	@BeforeClass
	public void setUp() throws Exception {
		init();
		selectBrowser("chrome");
	}

	@BeforeMethod
	public void login() throws Exception {
		LoginPage login = new LoginPage();
		login.LoginToAutospecs(driver);
		driverwait(10);
	}

	@Test
	public void Add_project() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 180);
		driverwait(10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Add_Project")))).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Project_Name"))))
				.sendKeys(prop.getProperty("Project_Name_value"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Project_Number"))))
				.sendKeys(prop.getProperty("Project_Number_value"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Version_Name"))))
				.sendKeys(prop.getProperty("Version_Name_value"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Project_Completion_Date"))))
				.sendKeys(prop.getProperty("ProjectCompletionDate_value"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Project_Type")))).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("ProjectType_value"))))
				.click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Project_Range")))).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("ProjectRange_value"))))
				.click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Create_Project")))).click();
        Thread.sleep(1000);
		WebElement element = driver.findElement(
				By.xpath("//body/div[3][@class='row ng-scope alert flash-notification alert-success']/span"));
		String str = element.getText();
		Assert.assertEquals("Project Successfully Created!!", str);
		System.out.println(str);
		Reporter.log(str);

		waitUntilElementIsVisible(driver, By.xpath(prop.getProperty("Choose_file_button")), 10);
		driver.findElement(By.xpath(prop.getProperty("Choose_file_button"))).click();

		Thread.sleep(1000);
		Reporter.log("Uploading Specs");
		StringSelection ss = new StringSelection(
				"/Autospecs_project/PDF_Upload/Scanned_file.pdf");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		waitUntilElementIsVisible(driver, By.xpath(prop.getProperty("UploadedSpecs_checkbox")), 10);

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("UploadedSpecs_checkbox"))))
				.click();
		waitUntilElementIsVisible(driver, By.xpath(prop.getProperty("Upload_button")), 15);
		driver.findElement(By.xpath(prop.getProperty("Upload_button"))).click();
		waitUntilElementIsVisible(driver, By.xpath(prop.getProperty("Validate_button")), 180);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Validate_button")))).click();
		waitUntilElementIsVisible(driver, By.xpath(prop.getProperty("Submittals_generated")), 180);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Submittals_generated"))))
				.click();
		waitUntilElementIsVisible(driver, By.xpath(prop.getProperty("GoTo_SmartRegister")), 180);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("GoTo_SmartRegister"))))
				.click();
		System.out.println("Navigated to smart register page");
		Reporter.log("Navigated to smart register page");
		String str1 = SR_footer();
		Thread.sleep(1000);
		Reporter.log("Total no. of records in Smart Register :" + str1);
	}

	@AfterClass
	public void closeBrowser() {
		driver.quit();
		Reporter.log("Browser Closed");

	}
}