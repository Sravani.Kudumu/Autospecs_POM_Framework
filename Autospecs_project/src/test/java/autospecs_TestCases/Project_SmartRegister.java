package autospecs_TestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pype.Autospecs.pageLibrary.LoginPage;
import com.pype.Autospecs.testbase.TestBase;

public class Project_SmartRegister extends TestBase {

	@BeforeClass
	public void setUp() throws Exception {
		init();
		selectBrowser("chrome");
	}

	@BeforeMethod
	public void login() throws Exception {
		LoginPage login = new LoginPage();
		login.LoginToAutospecs(driver);
		driverwait(10);
	}

	@Test
	public void Dashboard_page() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 180);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Project_SmartRegister"))))
				.click();
		Reporter.log("Navigated to Dashboard");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Select_project"))))
				.sendKeys(prop.getProperty("Project_Name_value"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("My_project")))).click();
		WebElement element = driver.findElement(By.xpath(".//*[@id='wrapper']/div[2]/h1"));
		String str = element.getText();
		Assert.assertEquals("Smart Register", str);
		Reporter.log(str);
		Reporter.log("Navigated to Smart Register page successfully");
		String str1=SR_footer();
		Reporter.log("Total no. of records in Smart Register :" +str1);
	}

	@AfterClass
	public void closeBrowser() {
		driver.quit();
		Reporter.log("Browser Closed");

	}
}
