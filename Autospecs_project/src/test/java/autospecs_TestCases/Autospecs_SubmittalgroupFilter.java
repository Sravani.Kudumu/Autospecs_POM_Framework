package autospecs_TestCases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pype.Autospecs.pageLibrary.LoginPage;
import com.pype.Autospecs.testbase.TestBase;

public class Autospecs_SubmittalgroupFilter extends TestBase {
	@BeforeClass
	public void setUp() throws Exception {
		init();
		selectBrowser("chrome");
	}

	@BeforeMethod
	public void login() throws Exception {
		LoginPage login = new LoginPage();
		login.LoginToAutospecs(driver);
		driverwait(10);
	}

	@Test
	public void Submittal_Group() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 180);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Search_project"))))
				.sendKeys(prop.getProperty("Project_Name_value"));
		Reporter.log("Project Selected");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("SmartRegister")))).click();
		Reporter.log("Clicked on Smart Register");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("SubmittalGroup_filter"))))
				.click();
		Reporter.log("Clicked on Submittal Group filter");
		List<WebElement> webElements = driver
				.findElements(By.xpath("//*[@class='popover-content']//*[@class='[ btn-group ]']"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Unselect_All")))).click();
		Reporter.log("Total no. of Submittal Groups in Filter : " + webElements.size());
		String s[] = new String[webElements.size()];
		for (int i = 0; i < webElements.size(); i++) {
			webElements = driver.findElements(By.xpath("//*[@class='popover-content']//*[@class='[ btn-group ]']"));
			s[i] = webElements.get(i).getText();
			webElements.get(i).click();
			Reporter.log("Submittal group :" + s[i]);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Apply_Filters")))).click();
			driverwait(10);
			if (i == 0) {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("SelectFrom_list")))).click();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("ClickOnSub_Group")))).click();
			}
			table();
			driver.findElement(By.cssSelector("button[popover-title='Select Submittal Groups to display']")).click();
			driver.findElement(By.xpath(prop.getProperty("Unselect_All"))).click();
		}
		Reporter.log("Submittal Group filter list is displayed successfully");
	}

	public void table() throws Exception {
		List<WebElement> Rows_table = driver
				.findElements(By.xpath(".//*[@id='grid_submittalRegisterGrid_records']//tbody/tr"));
		WebElement element = driver.findElement(By.xpath(".//*[@id='grid_submittalRegisterGrid_footer']/div/div[2]"));
		String str = element.getText();
		int i = str.indexOf("of");
		String str1 = str.substring(i + 3, str.length());
		Reporter.log("Total no. of records in the table : " + str1);
		int rows_count = Integer.parseInt(str1);
		int count = 1;
		for (int row = 3; row <= rows_count + 2; row++) {

			String gridVal = driver.findElement(By.xpath(".//*[@id='grid_submittalRegisterGrid_records']//tbody/tr[" + row + "]/td[12]")).getText();
		}

	}

	@AfterClass
	public void closeBrowser() {
		driver.quit();
		Reporter.log("Browser Closed");

	}
}
