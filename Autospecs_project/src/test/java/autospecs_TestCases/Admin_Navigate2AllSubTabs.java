package autospecs_TestCases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.pype.Autospecs.pageLibrary.LoginPage;
import com.pype.Autospecs.testbase.TestBase;

public class Admin_Navigate2AllSubTabs extends TestBase {
	@BeforeClass
	public void setUp() throws Exception {
		init();
		selectBrowser("chrome");
	}

	@BeforeMethod
	public void login() throws Exception {
		LoginPage login = new LoginPage();
		login.LoginToAutospecs(driver);
		driverwait(10);
	}

	@Test
	public void Autospecs_AllSubTabs() throws Exception {
		driverwait(5);
		List<WebElement> str = driver.findElements(By.xpath("//*[@id=\"sidebar-wrapper\"]/ul/li/a"));
		Reporter.log("Total no. of SubTabs for SuperAdmin:"+ str.size());
		String s[] = new String[str.size()];
		for (int i = 0; i < str.size(); i++) {
			s[i] = str.get(i).getText();
			Reporter.log("SubTabs :" + s[i]);
			}
			}
		
	
	@AfterClass
	 public void closeBrowser() {
		driver.quit();
		Reporter.log("Browser Closed");

	}
}