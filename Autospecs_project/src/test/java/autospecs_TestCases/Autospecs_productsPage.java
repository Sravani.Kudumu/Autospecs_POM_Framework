package autospecs_TestCases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.pype.Autospecs.pageLibrary.LoginPage;
import com.pype.Autospecs.testbase.TestBase;
import org.testng.Assert;

public class Autospecs_productsPage extends TestBase {
	@BeforeClass
	public void setUp() throws Exception {
		init();
		selectBrowser("chrome");
	}

	@BeforeMethod
	public void login() throws Exception {
		LoginPage login = new LoginPage();
		login.LoginToAutospecs(driver);
		Reporter.log("Login successfully");
		driverwait(10);
	}

	@Test

	public void products_page() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 180);
		driverwait(10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Search_project"))))
				.sendKeys(prop.getProperty("Project_Name_value"));
		Reporter.log("Project Selected");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("SmartRegister")))).click();
		Reporter.log("Clicked on Smart Register");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Products_Page")))).click();
		Reporter.log("Navigated to Products Page");
		Thread.sleep(2000);
		String str = PR_footer();
		Reporter.log("Total number of records in Product Register :" + str);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("SelectFirstRecord"))))
				.click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("SelectSecondRecord"))))
				.click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("Delete_Records")))).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("PD_Save_button")))).click();
		Thread.sleep(1000);
		WebElement element = driver.findElement(By.xpath("//body/div[1][@class='row ng-scope alert flash-notification alert-success']/span"));
		String str1 = element.getText();
		Thread.sleep(1000);
		Assert.assertEquals("Product Register Successfully Updated.", str1);
		Reporter.log("Two Records deleted successfully");
		Reporter.log(str1);
		Thread.sleep(2000);
		str=PR_footer();
		Reporter.log("Total number of records in Product Register :" + str);
		
	}

	@AfterClass
	public void closeBrowser() {
		driver.quit();
		Reporter.log("Browser Closed");

	}
}
