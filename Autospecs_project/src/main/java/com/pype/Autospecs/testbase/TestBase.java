package com.pype.Autospecs.testbase;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.pype.Autospecs.testUtills.Utills;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class TestBase extends Utills {

	public static Properties prop;
	public File f;
	public FileInputStream FI;
	
	public static ExtentReports extent;
	public static ExtentTest test;

	public void init() throws IOException {
		Reporter.log("Loading Properties into memory");
		loadPropertiesFile();
		Reporter.log("Browser is up and running");
		//driver =selectBrowser("chrome");
	}

	public void loadPropertiesFile() throws IOException {
		prop=new Properties();
		f = new File(System.getProperty("user.dir")
				+ "/src/main/java/com/pype/Autospecs/property_Files/Autospecs.properties");
		FI = new FileInputStream(f);
		prop.load(FI);

	}
	
	public String SR_footer() {
		WebElement element = driver.findElement(By.xpath(".//*[@id='grid_submittalRegisterGrid_footer']/div/div[2]"));
		String str = element.getText();
		int i = str.indexOf("of");
		String str1 = str.substring(i + 3, str.length());
		return str1;
		//System.out.println(str1);
	}
	public String PR_footer() {
		WebElement element = driver.findElement(By.xpath(".//*[@id='grid_submittalRegisterGrid_footer']/div/div[2]"));
		String str = element.getText();
		int i = str.indexOf("of");
		String str1 = str.substring(i + 3, str.length());
		return str1;
		//System.out.println(str1);
	}


}
