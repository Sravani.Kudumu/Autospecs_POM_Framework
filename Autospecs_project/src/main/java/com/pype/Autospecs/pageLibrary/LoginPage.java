package com.pype.Autospecs.pageLibrary;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import com.pype.Autospecs.testUtills.Utills;
import com.pype.Autospecs.testbase.TestBase;

public class LoginPage extends TestBase {
	static WebDriver driver = Utills.driver;

	public void LoginToAutospecs(WebDriver driver) throws Exception {

		driver.get(prop.getProperty("Test_url"));
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 180);
		Reporter.log("Autospecs Application is up and running");
		Reporter.log("Entering UserName..");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(prop.getProperty("Email"))))
				.sendKeys(prop.getProperty("Email_value"));
		driverwait(3);
		Reporter.log("Entering Password..");

		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(prop.getProperty("Password"))))
				.sendKeys(prop.getProperty("Password_value"));
		Reporter.log("Clicking on login..");
		driverwait(3);
		driver.findElement(By.xpath(prop.getProperty(("Login")))).click();

	}
	// Forcefully failed this test as to verify listener.

	public void TestToFail() {
		System.out.println("This method to test fail");
		Assert.assertTrue(false);
	}
}
